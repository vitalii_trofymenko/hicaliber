# Hicaliber test task

- **client** - client spa, based on Vue.js.
- **api** - server api, based on Laravel.
- **docker-compose.yaml** - configuration for docker-compose.

### Setup project
```
composer install
yarn install
```
- Make api/.env, based on an api/.env.example;
- Make client/.env.local, based on a client/.env.local.example;

### Start project
```
docker-compose up
```

### Run end-to-end tests
```
yarn test:e2e
docker exec -it hicaliber-client yarn test:e2e
```

### Run unit tests
```
api:
php artisan test
docker exec -it hicaliber-api php artisan test

client:
yarn test:unit
docker exec -it hicaliber-client yarn test:unit
```