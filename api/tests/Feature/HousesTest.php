<?php

namespace Tests\Feature;

use App\Models\House;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HousesTest extends TestCase
{
    use RefreshDatabase;

    private $sampleHouse;

    public function setUp(): void
    {
        parent::setUp();
        $this->sampleHouse = House::factory()->create();
    }

    public function test_search_houses_by_name()
    {
        $this->makeSearchRequest(['name' => $this->sampleHouse->name]);
    }

    public function test_search_houses_by_price()
    {
        $this->makeSearchRequest(['price_from' => $this->sampleHouse->price, 'price_to' => $this->sampleHouse->price]);
    }

    public function test_search_houses_by_bedrooms()
    {
        $this->makeSearchRequest(['bedrooms' => $this->sampleHouse->bedrooms]);
    }

    public function test_search_houses_by_bathrooms()
    {
        $this->makeSearchRequest(['bathrooms' => $this->sampleHouse->bathrooms]);
    }

    public function test_search_houses_by_storeys()
    {
        $this->makeSearchRequest(['storeys' => $this->sampleHouse->storeys]);
    }

    public function test_search_houses_by_garages()
    {
        $this->makeSearchRequest(['garages' => $this->sampleHouse->garages]);
    }

    private function makeSearchRequest(array $params) {
        $response = $this->json('GET', route('houses.search'), $params);
        $response->assertSuccessful();
        $response->assertSee($this->sampleHouse->id);
    }
}
