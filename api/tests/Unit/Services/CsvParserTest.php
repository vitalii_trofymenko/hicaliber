<?php

namespace Tests\Unit\Services;

use App\Services\CsvParser;
use Tests\TestCase;

class CsvParserTest extends TestCase
{
    const FILE = 'property-data.csv';
    private $parser;

    public function setUp(): void
    {
        parent::setUp();
        $this->parser = new CsvParser(storage_path(self::FILE));
    }

    public function test_the_parser_can_parse_data()
    {
        $data = $this->parser->parse()->current();

        $this->assertIsArray($data);
        $this->assertNotEmpty($data);
    }
}
