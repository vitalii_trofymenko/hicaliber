<?php

namespace Database\Seeders;

use App\Repositories\HouseRepositoryInterface;
use App\Services\CsvParser;
use Illuminate\Database\Seeder;

class HousesTableSeeder extends Seeder
{
    const FILE = 'property-data.csv';
    const MAPPING = [
        'name',
        'price',
        'bedrooms',
        'bathrooms',
        'storeys',
        'garages',
    ];

    private $houseRepository;

    public function __construct(HouseRepositoryInterface $houseRepository)
    {
        $this->houseRepository = $houseRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $parser = new CsvParser(storage_path(self::FILE));
            $parser->parse()->current();
            foreach ($parser->parse() as $row) {
                if ($row) {
                    $this->houseRepository->create($this->mapData($row));
                }
            }
        } catch (\Exception $exception) {
            $this->command->error($exception->getMessage());
        }
    }

    private function mapData(array $data): array
    {
        return array_combine(self::MAPPING, $data);
    }
}
