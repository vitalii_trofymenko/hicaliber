<?php

namespace Database\Factories;

use App\Models\House;
use Illuminate\Database\Eloquent\Factories\Factory;

class HouseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = House::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->city,
            'price' => $this->faker->randomDigit,
            'bedrooms' => $this->faker->randomDigit,
            'bathrooms' => $this->faker->randomDigit,
            'storeys' => $this->faker->randomDigit,
            'garages' => $this->faker->randomDigit,
        ];
    }
}
