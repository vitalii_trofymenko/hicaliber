<?php

namespace App\Services;

use App\Repositories\HouseRepositoryInterface;

class HouseService {
    protected $houseRepository;

    public function __construct(HouseRepositoryInterface $houseRepository)
    {
        $this->houseRepository = $houseRepository;
    }

    public function getAllByFilters(array $filters)
    {
        return $this->houseRepository->getAllByFilters($filters);
    }
}
