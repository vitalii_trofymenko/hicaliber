<?php

namespace App\Services;

class CsvParser {
    protected $file;

    public function __construct(string $file)
    {
        if (pathinfo($file)['extension'] !== 'csv') {
            throw new \Exception('Invalid file extension.');
        }
        if (!$this->file = fopen($file, 'r')) {
            throw new \Exception('File opening has been failed.');
        }
    }

    public function parse(): \Generator
    {
        while (!feof($this->file)) {
            yield fgetcsv($this->file, 0, ',');
        }
    }
}
