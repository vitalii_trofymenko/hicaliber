<?php

namespace App\Repositories;

interface HouseRepositoryInterface {
    public function create(array $attributes);
    public function getAllByFilters(array $filters);
}
