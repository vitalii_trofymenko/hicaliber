<?php

namespace App\Repositories\Eloquent;

use App\Models\House;
use App\Repositories\HouseRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class HouseRepository implements HouseRepositoryInterface
{
    public function create(array $attributes): Model
    {
        return House::query()->create($attributes);
    }

    public function getAllByFilters(array $filters): Collection
    {
        $houses = House::query();

        if (isset($filters['name'])) {
            $houses->where('name', 'like', "%{$filters['name']}%");
        }
        if (isset($filters['price_from'])) {
            $houses->where('price', '>=', $filters['price_from']);
        }
        if (isset($filters['price_to'])) {
            $houses->where('price', '<=', $filters['price_to']);
        }
        if (isset($filters['bedrooms'])) {
            $houses->where('bedrooms', '=', $filters['bedrooms']);
        }
        if (isset($filters['bathrooms'])) {
            $houses->where('bathrooms', '=', $filters['bathrooms']);
        }
        if (isset($filters['storeys'])) {
            $houses->where('storeys', '=', $filters['storeys']);
        }
        if (isset($filters['garages'])) {
            $houses->where('garages', '=', $filters['garages']);
        }

        return $houses->get();
    }
}
