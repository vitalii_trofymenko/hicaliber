<?php

namespace App\Providers;

use App\Repositories\Eloquent\HouseRepository;
use App\Repositories\HouseRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HouseRepositoryInterface::class, HouseRepository::class);
    }
}
