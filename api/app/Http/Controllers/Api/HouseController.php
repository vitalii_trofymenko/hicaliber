<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SearchHousesRequest;
use App\Http\Resources\Api\HouseResource;
use App\Services\HouseService;

class HouseController extends Controller
{
    protected $houseService;

    public function __construct(HouseService $houseService)
    {
        $this->houseService = $houseService;
    }

    public function search(SearchHousesRequest $request)
    {
        $houses = $this->houseService->getAllByFilters($request->all());

        return response()->json([
            'data' => HouseResource::collection($houses),
        ], 200);
    }
}
