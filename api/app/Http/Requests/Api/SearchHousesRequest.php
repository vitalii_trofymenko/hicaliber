<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SearchHousesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'sometimes|required|string',
            'price_from' => 'sometimes|required|numeric',
            'price_to' => 'sometimes|required|numeric',
            'bedrooms' => 'sometimes|required|integer|max:10',
            'bathrooms' => 'sometimes|required|integer|max:10',
            'storeys' => 'sometimes|required|integer|max:10',
            'garages' => 'sometimes|required|integer|max:10',
        ];
    }
}
