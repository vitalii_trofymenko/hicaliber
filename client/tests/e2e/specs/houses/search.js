describe('Houses Searching', () => {
  beforeEach(() => {
    cy.fixture('searchHousesData.json').as('searchData');
    cy.fixture('sampleHousesData.json').as('sampleHousesData');
  });

  it('Visits the app root url', () => {
    cy.visit('/');
    cy.get('form').contains('form', 'Search');
  });

  it('Fill in the form', () => {
    cy.get('@searchData').then(data => {
      cy.get('input[name="name"]').type(data.name);
      cy.get('input[name="price_from"]').type(data.price_from);
      cy.get('input[name="price_to"]').type(data.price_to);
      cy.get('input[name="bedrooms"]').type(data.bedrooms);
      cy.get('input[name="bathrooms"]').type(data.bathrooms);
      cy.get('input[name="storeys"]').type(data.storeys);
      cy.get('input[name="garages"]').type(data.garages);
    });
  });

  it('Submit the form', () => {
    cy.intercept({ method: 'GET' }, (req) => {
      req.reply((res) => {
        res.send({ fixture: 'sampleHousesData.json' });
      });
    }).as('searchRequest');
    cy.contains('button', 'Search').click();
    cy.wait('@searchRequest').then(({ request }) => {
      cy.get('@searchData').then(data => {
        const urlSearchParams = new URLSearchParams();
        Object.keys(data).forEach(key => urlSearchParams.append(key, data[key]));
        expect(request.url.includes(urlSearchParams.toString())).to.be.true;
      });
    });
  });

  it('Show loader on data loading', () => {
    cy.intercept({ method: 'GET' }).as('searchRequestWithDelay');

    cy.contains('button', 'Search').click();
    cy.get('.el-loading-mask').should('exist');
    cy.wait('@searchRequestWithDelay').then(() => {
      cy.get('.el-loading-mask', { timeout: 500 }).should('not.exist');
    });
  });
});
