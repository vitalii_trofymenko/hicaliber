import { mount } from '@vue/test-utils';
import ElementPlus from 'element-plus';
import HousesSearchForm from '@/components/HousesSearchForm';

describe('HousesSearchForm.vue', () => {
  const wrapper = mount(HousesSearchForm, {
    global: {
      plugins: [
        ElementPlus,
      ],
    },
  });

  it('should emit validation event on submit form', () => {
    wrapper.find('form').find('button').trigger('click');
    expect(wrapper.emitted()).toHaveProperty('validate');
  });

  it('should emit submit event on submit form', async () => {
    await wrapper.find('form').find('button').trigger('click');
    expect(wrapper.emitted()).toHaveProperty('submit');
    expect(Array.isArray(wrapper.emitted('submit')[0])).toBeTruthy();

    await wrapper.find('form').trigger('keyup.enter');
    expect(wrapper.emitted('submit')).toHaveLength(3);
  });
});
