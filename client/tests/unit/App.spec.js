import { shallowMount } from '@vue/test-utils';
import ElementPlus from 'element-plus';
import flushPromises from 'flush-promises';
import App from '@/App';

const mockFakeHouses = [
  { name: 'Test 1' },
  { name: 'Test 2' },
];
const mockAxios = {
  create: jest.fn(),
  get: jest.fn(() => ({ data: { data: mockFakeHouses } })),
};

describe('App.vue', () => {
  it('should make search houses request with loader', async () => {
    const wrapper = shallowMount(App, {
      global: {
        plugins: [
          ElementPlus,
        ],
        mocks: {
          axios: mockAxios,
        }
      },
    });

    wrapper.vm.search(mockFakeHouses[0]);

    expect(wrapper.vm.$data.state.isLoading).toBeTruthy();
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith('api/v1/houses/search', { params: mockFakeHouses[0] });

    await flushPromises();

    expect(wrapper.vm.$data.houses).toHaveLength(mockFakeHouses.length);
    expect(wrapper.vm.$data.state.isLoading).toBeFalsy();
  });
});
