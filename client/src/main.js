import { createApp } from 'vue';
import App from './App.vue';
import axios from './plugins/axios';
import installElementPlus from './plugins/element';

const app = createApp(App);
app.use(axios);
installElementPlus(app);
app.mount('#app');
