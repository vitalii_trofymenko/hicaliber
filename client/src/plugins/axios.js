import axios from 'axios';

const config = Object.freeze({
  baseURL: process.env.VUE_APP_API_URL,
});

const _axios = axios.create(config);

export default {
  install: (app) => {
    app.config.globalProperties.axios = _axios;
  }
}
